`self_localize_and_map_by_lidar` behavior package for sending and receiving data from the hector slam package

This behavior requires the `hector_mapping` node to be running, an example can be found in [hector_mapping.launch file](./behavior_self_localize_and_map_by_lidar/launch/hector_mapping_example.launch)

