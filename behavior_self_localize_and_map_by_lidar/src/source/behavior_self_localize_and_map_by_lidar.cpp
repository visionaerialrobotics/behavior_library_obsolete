/*!*******************************************************************************************
 *  \file       behavior_self_localize_and_map_by_lidar.cpp
 *  \brief      Behavior Self Localize and Map by Lidar implementation file.
 *  \details    This file implements the BehaviorSelfLocalizeAndMapByLidar class.
 *  \authors    Hriday Bavle
 *  \maintainer Guillermo Echegoyen
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#include "../include/behavior_self_localize_and_map_by_lidar.h"

BehaviorSelfLocalizeAndMapByLidar::BehaviorSelfLocalizeAndMapByLidar() : BehaviorProcess()
{
  hector_reset_msg.data = "reset";
}

BehaviorSelfLocalizeAndMapByLidar::~BehaviorSelfLocalizeAndMapByLidar()
{
}

void BehaviorSelfLocalizeAndMapByLidar::ownSetUp(){
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory, "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack");

  private_nh.param<std::string>("hector_map_topic", hector_map_topic_str, "hector_map");
  private_nh.param<std::string>("output_map_topic", output_map_topic_str, "hector_map");

  is_stopped = false;
}


void BehaviorSelfLocalizeAndMapByLidar::ownStart(){
  //Subscribers
  hectorMapSub  = node_handle.subscribe(hector_map_topic_str, 1, &BehaviorSelfLocalizeAndMapByLidar::hectorMapCallback, this);
  mapPub = node_handle.advertise<nav_msgs::OccupancyGrid>(output_map_topic_str, 1, false);

  hectorReset = node_handle.advertise<std_msgs::String>("syscommand", 1, false);

  is_stopped = false;
  // start processes is done automatically by the parent class
  hectorReset.publish(hector_reset_msg);
}

void BehaviorSelfLocalizeAndMapByLidar::ownRun(){}
void BehaviorSelfLocalizeAndMapByLidar::ownStop(){
  is_stopped = true;
  hectorMapSub.shutdown();
  mapPub.shutdown();
  // stop process to droneRobotLocalizationROS and ekf_localization is done automatically by the parent class
  hectorReset.publish(hector_reset_msg);
}

std::tuple<bool,std::string> BehaviorSelfLocalizeAndMapByLidar::ownCheckActivationConditions(){
  // ToDo := Check lidar, battery & flying state
  return std::make_tuple(true,"");
}

void BehaviorSelfLocalizeAndMapByLidar::hectorMapCallback(const nav_msgs::OccupancyGrid &msg) {
  if( !is_stopped ){
    mapPub.publish(msg);
  }
}

