/*!*******************************************************************************************
*  \file       behavior_self_localize_and_map_by_lidar.h
*  \brief      behavior self localize and map by lidar definition file.
*  \details     This file contains the BehaviorSelfLocalizeAndMapByLidar declaration. To obtain more information about
*              it's definition consult the behavior_self_localize_and_map_by_lidar.cpp file.
*  \authors    Hriday Bavle
*  \maintainer Guillermo Echegoyen
*  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM)
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program. If not, see http://www.gnu.org/licenses/.
********************************************************************************/
#ifndef BEHAVIOR_SELF_LOCALIZE_AND_MAP_BY_LIDAR_H
#define BEHAVIOR_SELF_LOCALIZE_AND_MAP_BY_LIDAR_H

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <sstream>

// ROS
#include <ros/ros.h>

#include <sensor_msgs/LaserScan.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/OccupancyGrid.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/RequestProcesses.h>
#include <behavior_process.h>


class BehaviorSelfLocalizeAndMapByLidar : public BehaviorProcess
{
public:
  BehaviorSelfLocalizeAndMapByLidar();
  ~BehaviorSelfLocalizeAndMapByLidar();

private:
  ros::NodeHandle node_handle;

  ros::Subscriber hectorMapSub;
  ros::Publisher mapPub;
  
  ros::Publisher hectorReset;
  std_msgs::String hector_reset_msg;

  void hectorMapCallback(const nav_msgs::OccupancyGrid& msg);

  //Congfig variables
  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;

  std::string hector_map_topic_str;
  std::string output_map_topic_str;

  bool is_stopped;

  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
  std::tuple<bool,std::string> ownCheckActivationConditions();

};

#endif
