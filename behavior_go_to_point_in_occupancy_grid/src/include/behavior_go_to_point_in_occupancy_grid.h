/*!*******************************************************************************************
*  \file       behavior_go_to_point_in_occupancy_grid.h
*  \brief      behavior go to point in occupancy grid definition file.
*  \details     This file contains the BehaviorGoToPointInOccupancyGrid declaration. To obtain more information about
*              it's definition consult the behavior_go_to_point_in_occupancy_grid.cpp file.
*  \authors    Guillermo Echegoyen
*  \maintainer Guillermo Echegoyen
*  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program. If not, see http://www.gnu.org/licenses/.
********************************************************************************/
#ifndef BEHAVIOR_GO_TO_POINT_IN_OCCUPANCY_GRID_H
#define BEHAVIOR_GO_TO_POINT_IN_OCCUPANCY_GRID_H

#include <string>

// ROS
#include <ros/ros.h>

#include <nav_msgs/Path.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <droneMsgsROS/PathWithID.h>
#include <droneMsgsROS/GeneratePath.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>
#include <droneMsgsROS/dronePositionRefCommand.h>
#include <droneMsgsROS/droneCommand.h>
#include <behavior_process.h>
#include <yaml-cpp/yaml.h>


class BehaviorGoToPointInOccupancyGrid : public BehaviorProcess
{
public:
  BehaviorGoToPointInOccupancyGrid();
  ~BehaviorGoToPointInOccupancyGrid();

private:

  // Maximum difference between estimated and requested yaw
  const double YAW_TOL = 0.1;
  const double DIST_TOL = 0.2;

  ros::NodeHandle node_handle;
  // Ros publishers & subrcribers
  ros::Subscriber estimated_pose_sub;
  ros::Subscriber path_sub;
  ros::Publisher yaw_controller_pub;
  ros::Publisher trajectory_pub;
  ros::Publisher controllers_pub;
  ros::ServiceClient belief_manager_client;
  ros::ServiceClient path_client;

  // Congfig variables
  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;
  
  std::string generated_path_topic_str;
  std::string generate_path_service_str;
  std::string estimated_pose_str;
  std::string yaw_controller_str;
  std::string controllers_topic;
  std::string trajectory_topic;
  std::string execute_query_srv;

  // State variables
  bool is_finished;
  bool bad_args;
  bool changing_yaw;
  bool pending_path;
  bool obstacle;
  bool yaw_sent;
  double current_yaw;
  double target_yaw;
  unsigned int path_id;
  droneMsgsROS::dronePose estimated_pose;
  droneMsgsROS::dronePose target_position;
  droneMsgsROS::dronePositionTrajectoryRefCommand current_traj;

  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
  std::tuple<bool,std::string> ownCheckActivationConditions();

  // Callbacks
  void generatedPathCallback(const droneMsgsROS::PathWithID &resp_path);
  void estimatedPoseCallBack(const droneMsgsROS::dronePose& msg);
  // Utils - Grabbed from generate path in occupancy grid
  void convertPath(const nav_msgs::Path &path, droneMsgsROS::dronePositionTrajectoryRefCommand &return_path);
  bool grabInputPose(const droneMsgsROS::dronePose estimated_pose, droneMsgsROS::dronePose &position);
  int requestPath(droneMsgsROS::dronePose position);
  // Utils - Grabbed from FollowPathInOccupancyGrid
  void setupTrajectory(droneMsgsROS::dronePositionTrajectoryRefCommand &return_path);
  void setupTopics();
  void setupStartPosition(droneMsgsROS::dronePose &position);
  void setControllerMode();
  bool goalReached(droneMsgsROS::dronePose current, droneMsgsROS::dronePose target);
  double fixYaw(double inputYaw);
  void sendTrajectory(droneMsgsROS::dronePositionTrajectoryRefCommand &traj);
  void sendYaw(double yaw);
  void yawOrRequest();
  void dumpTopicNames();
};

#endif
