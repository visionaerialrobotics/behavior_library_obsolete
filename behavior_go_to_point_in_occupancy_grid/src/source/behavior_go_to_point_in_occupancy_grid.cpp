/*!*******************************************************************************************
 *  \file       behavior_go_to_point_in_occupancy_grid.cpp
 *  \brief      Behavior Go To Point In Occupancy Grid implementation file.
 *  \details    This file implements the BehaviorGoToPointInOccupancyGrid class.
 *  \authors    Guillermo Echegoyen
 *  \maintainer Guillermo Echegoyen
 *  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#include "../include/behavior_go_to_point_in_occupancy_grid.h"

BehaviorGoToPointInOccupancyGrid::BehaviorGoToPointInOccupancyGrid() : BehaviorProcess(){}
BehaviorGoToPointInOccupancyGrid::~BehaviorGoToPointInOccupancyGrid(){}

void BehaviorGoToPointInOccupancyGrid::ownSetUp(){
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory, "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack");

  private_nh.param<std::string>("generated_path_topic", generated_path_topic_str, "path_with_id");
  private_nh.param<std::string>("generate_path_service", generate_path_service_str, "generate_path");
  private_nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "EstimatedPose_droneGMR_wrt_GFF");
  private_nh.param<std::string>("yaw_controller_topic", yaw_controller_str , "droneControllerYawRefCommand");
  private_nh.param<std::string>("trajectory_topic", trajectory_topic, "droneTrajectoryAbsRefCommand");
  private_nh.param<std::string>("consult_belief", execute_query_srv,"consult_belief");
  private_nh.param<std::string>("controllers_topic", controllers_topic, "command/high_level");
  dumpTopicNames();
}

void BehaviorGoToPointInOccupancyGrid::ownStart(){
  std::cout << "ownStart" << std::endl;

  is_finished = false;
  bad_args = false;
  obstacle = false;
  changing_yaw = false;
  pending_path = true;
  yaw_sent = false;

  setupTopics();
  setupStartPosition(estimated_pose);

  // Extract target position
  if( !grabInputPose(estimated_pose, target_position) ){
    setStarted(false);
    bad_args = true;
    is_finished = true;
    return;
  }

  setStarted(true);

  target_yaw = fixYaw(target_position.yaw);
  current_yaw = fixYaw(estimated_pose.yaw);
  
  changing_yaw = false;
  path_id = requestPath(target_position);
  return;

  yawOrRequest();
}

void BehaviorGoToPointInOccupancyGrid::ownRun(){
  is_finished = goalReached(estimated_pose, target_position);
  if( timerIsFinished() || (is_finished && bad_args) ){
    std::cout<< "Behavior finished WITH error" << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }else if( obstacle || is_finished ){
    std::cout<< "Behavior finished WITHOUT error" << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }
}

void BehaviorGoToPointInOccupancyGrid::ownStop()
{
  path_sub.shutdown();
  path_client.shutdown();
  estimated_pose_sub.shutdown();
  yaw_controller_pub.shutdown();
  trajectory_pub.shutdown();
  belief_manager_client.shutdown();
  controllers_pub.shutdown();
}

std::tuple<bool,std::string> BehaviorGoToPointInOccupancyGrid::ownCheckActivationConditions(){
  
  droneMsgsROS::ConsultBelief query_service;
  query_service.request.query = "battery_level(self,LOW)";

  if( belief_manager_client.call(query_service) == 0 ){
    std::cout<<"Warning: Belief Query service not ready, try again..."<<std::endl;
  }

  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
  }

  query_service.request.query = "flight_state(self,LANDED)";

  if( belief_manager_client.call(query_service) == 0 ){
    std::cout<<"Warning: Belief Query service not ready, try again..."<<std::endl;
  }

  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
  }
  return std::make_tuple(true,"");
}

// Callbacks
void BehaviorGoToPointInOccupancyGrid::generatedPathCallback(const droneMsgsROS::PathWithID &resp_path){
  std::cout<< "Path is back id (" << resp_path.uid << "), our id (" << path_id << ")" <<std::endl;
  if( resp_path.uid != path_id ){
    // not for us
    return;
  }
  std::cout<< "Path is back size " << resp_path.poses.size() <<"\n";
  // No need to convert, just reusability
  nav_msgs::Path nav_path;
  nav_path.poses = resp_path.poses;
  setupTrajectory(current_traj);
  if( nav_path.poses.size() > 0 ){
    convertPath(nav_path, current_traj);
  }else{
    // Obstacle, stop!
    obstacle = true;
    droneMsgsROS::dronePositionRefCommand waypoint;
    waypoint.x = estimated_pose.x;
    waypoint.y = estimated_pose.y;
    waypoint.z = estimated_pose.z;
    // waypoint.yaw = target_yaw;
    current_traj.droneTrajectory.push_back(waypoint);
  }
  sendTrajectory(current_traj);
  // BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
  // BehaviorProcess::setFinishConditionSatisfied(true);
}

void BehaviorGoToPointInOccupancyGrid::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg){
  estimated_pose = msg;
  current_yaw = fixYaw(estimated_pose.yaw);
  /*if( is_finished = goalReached(estimated_pose, target_position) ){
    return;
  }*/
  /*if( pending_path || changing_yaw ){
    if( !yaw_sent ){
      yawOrRequest();
      sendYaw(target_yaw);
    }
  }*/
  // else executing trajectory
}

// Convert a navigation path to an aerostack one
void BehaviorGoToPointInOccupancyGrid::convertPath(
  const nav_msgs::Path &path,
  droneMsgsROS::dronePositionTrajectoryRefCommand &return_path
){
  for(int i = 0; i < path.poses.size(); i++){
    droneMsgsROS::dronePositionRefCommand next_waypoint;
    next_waypoint.x = path.poses[i].pose.position.x;
    next_waypoint.y = path.poses[i].pose.position.y;
    next_waypoint.z = target_position.z;
    // ToDo := Is yaw necessary here?
    return_path.droneTrajectory.push_back(next_waypoint);
  }
}

// Extract input arguments
bool BehaviorGoToPointInOccupancyGrid::grabInputPose(const droneMsgsROS::dronePose estimated_pose, droneMsgsROS::dronePose &position){
  // Extract target position
  std::string arguments = getArguments();
  YAML::Node config_file = YAML::Load(arguments);
  if(!config_file["coordinates"].IsDefined() && !config_file["relative_coordinates"].IsDefined()){
    std::cout<< "Null point" <<std::endl;
    return false;
  }else if( config_file["coordinates"].IsDefined() ){
    std::vector<double> points = config_file["coordinates"].as<std::vector<double>>();
    position.x   = std::isinf(points[0]) ? estimated_pose.x   : points[0];
    position.y   = std::isinf(points[1]) ? estimated_pose.y   : points[1];
    position.z   = std::isinf(points[2]) ? estimated_pose.z   : points[2];
    position.yaw = std::isinf(points[3]) ? estimated_pose.yaw : points[3];
    std::cout<< "Got point ["<<position.x << ", "<< position.y << ", " << position.z << ", " << position.yaw <<"]"<<std::endl;
  }else{
    std::vector<double> points = config_file["relative_coordinates"].as<std::vector<double>>();
    position.x   = std::isinf(points[0]) ? estimated_pose.x    : points[0] + estimated_pose.x;
    position.y   = std::isinf(points[1]) ? estimated_pose.y    : points[1] + estimated_pose.y;
    position.z   = std::isinf(points[2]) ? estimated_pose.z    : points[2] + estimated_pose.z;
    position.yaw = std::isinf(points[3]) ? estimated_pose.yaw  : points[3] + estimated_pose.yaw;
    std::cout<< "Got Relative point ["<<position.x << ", "<< position.y << ", " << position.z << ", " << position.yaw <<"]"<<std::endl;
  }
  return true;
}

// Send point to request path
int BehaviorGoToPointInOccupancyGrid::requestPath(droneMsgsROS::dronePose position){

  // ask for a path to be generated
  droneMsgsROS::GeneratePath path_msg;

  geometry_msgs::PoseStamped target_stamped;
  target_stamped.pose.position.x = position.x;
  target_stamped.pose.position.y = position.y;
  target_stamped.pose.position.z = position.z;

  // ToDo := Yaw necessary?

  path_msg.request.goal = target_stamped;
  path_client.call(path_msg);
  int id = path_msg.response.uid;

  pending_path = false;

  std::cout<< "Run on point, got id "<< id << " name " << path_client.getService() <<std::endl;
  return id;
}


void BehaviorGoToPointInOccupancyGrid::setControllerMode(){
  // set trajectory controller mode to move and wait for the change
  droneMsgsROS::droneCommand msg;
  std_msgs::Header header;
  header.frame_id = "behavior_go_to_point_in_occupancy_grid";
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);

  // Wait for controller to change mode
  ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
    "droneTrajectoryController/controlMode", node_handle
  );
}

void BehaviorGoToPointInOccupancyGrid::setupTrajectory(
  droneMsgsROS::dronePositionTrajectoryRefCommand &return_path
){
  return_path = droneMsgsROS::dronePositionTrajectoryRefCommand();
  return_path.header.stamp = ros::Time::now();
  return_path.initial_checkpoint = 0;
  return_path.is_periodic = false;
  return_path.droneTrajectory.clear();
}

void BehaviorGoToPointInOccupancyGrid::setupTopics(){
  // Initialize topics
  path_sub = node_handle.subscribe(generated_path_topic_str, 1, &BehaviorGoToPointInOccupancyGrid::generatedPathCallback, this);
  path_client = node_handle.serviceClient<droneMsgsROS::GeneratePath>(generate_path_service_str);
  estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &BehaviorGoToPointInOccupancyGrid::estimatedPoseCallBack, this);
  yaw_controller_pub = node_handle.advertise<droneMsgsROS::droneYawRefCommand>(yaw_controller_str, 1);
  trajectory_pub = node_handle.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(trajectory_topic, 1);
  belief_manager_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);
  controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_topic, 1000, true);
}

void BehaviorGoToPointInOccupancyGrid::setupStartPosition(droneMsgsROS::dronePose &position){
  // Get an initial estimate, just in case of a relative point
  position.x = 0;
  position.y = 0;
  position.z = 0;
  position = *ros::topic::waitForMessage<droneMsgsROS::dronePose>(estimated_pose_str, node_handle);
}

void BehaviorGoToPointInOccupancyGrid::sendTrajectory(droneMsgsROS::dronePositionTrajectoryRefCommand &traj){
  setControllerMode();
  std::cout<<"Trajectory sent with (" << traj.droneTrajectory.size() << ") points"<<std::endl;
  trajectory_pub.publish(traj);
}

double BehaviorGoToPointInOccupancyGrid::fixYaw(double in_yaw){
  double ret_yaw = in_yaw;
  if( in_yaw < 0){
    ret_yaw += 2 * M_PI;  //No angles lower < 0 
  }
  return ret_yaw;
}

void BehaviorGoToPointInOccupancyGrid::sendYaw(double yaw){
  setControllerMode();
  changing_yaw = true;
  // Ask for a move in yaw
  droneMsgsROS::droneYawRefCommand final_yaw;
  final_yaw.header.stamp = ros::Time::now();
  final_yaw.yaw = yaw;
  yaw_controller_pub.publish(final_yaw);
  yaw_sent = true;
}

bool BehaviorGoToPointInOccupancyGrid::goalReached(droneMsgsROS::dronePose current, droneMsgsROS::dronePose target){
  /*std::cout<<"Goal "<< fabs(current.x - target.x);
  std::cout<<", "<< fabs(current.y - target.y);
  std::cout<<", "<< fabs(current.z - target.z)<<std::endl;*/
  return (
    fabs(current.x - target.x) < DIST_TOL &&
    fabs(current.y - target.y) < DIST_TOL &&
    fabs(current.z - target.z) < DIST_TOL
    // && fabs(current.yaw - target.yaw) < YAW_TOL
  );
}

void BehaviorGoToPointInOccupancyGrid::yawOrRequest(){
  if( fabs(target_yaw - current_yaw) > YAW_TOL ){
    std::cout<< "Correcting yaw first..." << std::endl;
    std::cout<< "Requested (" << target_yaw << ") current(" << current_yaw << ")" << std::endl;
    changing_yaw = true;
    // Sleep a moment and ask again
    // sendYaw(target_yaw);
    // if( fabs(target_yaw - current_yaw) > YAW_TOL){
    //   ros::spinOnce();
    // }
  }else if( pending_path ){
    std::cout<< "Requesting path" << std::endl;
    changing_yaw = false;
    path_id = requestPath(target_position);
  }
}

void BehaviorGoToPointInOccupancyGrid::dumpTopicNames(){
  std::cout<<"generated_path_topic_str "<<generated_path_topic_str<<std::endl;
  std::cout<<"generate_path_service_str "<<generate_path_service_str<<std::endl;
  std::cout<<"estimated_pose_str "<<estimated_pose_str<<std::endl;
  std::cout<<"yaw_controller_str "<<yaw_controller_str<<std::endl;
  std::cout<<"trajectory_topic "<<trajectory_topic<<std::endl;
  std::cout<<"execute_query_srv "<<execute_query_srv<<std::endl;
  std::cout<<"controllers_topic "<<controllers_topic<<std::endl;
}
