/*!*******************************************************************************************
 *  \file       behavior_go_to_point_in_occupancy_grid.cpp
 *  \brief      behavior go to point in occupancy grid main file.
 *  \details    This file contains the BehaviorGoToPointInOccupancyGrid main file.
 *  \authors    Guillermo Echegoyen
 *  \maintainer Guillermo Echegoyen
 *  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#include "../include/behavior_go_to_point_in_occupancy_grid.h"
#include <boost/thread/thread.hpp>

int main(int argc, char** argv){
  ros::init(argc, argv, ros::this_node::getName());

  std::cout << ros::this_node::getName() << std::endl;

  BehaviorGoToPointInOccupancyGrid behavior;
  behavior.setUp();

  // 1/4 second sleep
  ros::Rate rate(30);
  while(ros::ok()){
    ros::spinOnce();
    behavior.run();
    rate.sleep();
  }
  return 0;
}
