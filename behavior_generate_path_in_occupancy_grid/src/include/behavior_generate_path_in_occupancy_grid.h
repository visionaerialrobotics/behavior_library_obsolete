/*!*******************************************************************************************
*  \file       behavior_generate_path_in_occupancy_grid.h
*  \brief      behavior generate path in occupancy grid definition file.
*  \details     This file contains the BehaviorGeneratePathInOccupancyGrid declaration. To obtain more information about
*              it's definition consult the behavior_generate_path_in_occupancy_grid.cpp file.
*  \authors    Guillermo Echegoyen
*  \maintainer Guillermo Echegoyen
*  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program. If not, see http://www.gnu.org/licenses/.
********************************************************************************/
#ifndef BEHAVIOR_GENERATE_PATH_IN_OCCUPANCY_GRID_H
#define BEHAVIOR_GENERATE_PATH_IN_OCCUPANCY_GRID_H

#include <string>

// ROS
#include <ros/ros.h>

#include <nav_msgs/Path.h>
#include <droneMsgsROS/AddBelief.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/PathWithID.h>
#include <droneMsgsROS/GeneratePath.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/dronePositionRefCommand.h>
#include <droneMsgsROS/GenerateID.h>
#include <behavior_process.h>
#include <yaml-cpp/yaml.h>

class BehaviorGeneratePathInOccupancyGrid : public BehaviorProcess
{
public:
  BehaviorGeneratePathInOccupancyGrid();
  ~BehaviorGeneratePathInOccupancyGrid();

private:

  // Publishers, subscribers & servers
  ros::NodeHandle node_handle;
  ros::Subscriber path_sub;
  ros::ServiceClient path_client;
  ros::ServiceClient belief_manager_client;
  ros::ServiceClient generate_id_client;

  unsigned int path_id;
  bool is_finished;
  bool bad_args;

  droneMsgsROS::dronePose target_position;

  //Congfig variables
  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;

  std::string generated_path_topic_str;
  std::string generate_path_service_str;
  std::string add_belief_service_str;

  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
  std::tuple<bool,std::string> ownCheckActivationConditions();

  void generatedPathCallback(const droneMsgsROS::PathWithID &path);

  // Utils
  int requestBeliefId();
  void convertPath(const nav_msgs::Path &path, droneMsgsROS::dronePositionTrajectoryRefCommand &return_path);
  std::string serializePath(int id, droneMsgsROS::dronePositionTrajectoryRefCommand &path);
  void addBelief(std::string belief);
  bool grabInputPose(droneMsgsROS::dronePose &position);
  int requestPath(droneMsgsROS::dronePose position);
};

#endif
