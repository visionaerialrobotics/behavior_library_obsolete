/*!*******************************************************************************************
 *  \file       behavior_follow_path_in_occupancy_grid.cpp
 *  \brief      Behavior Follow Path In Occupancy Grid implementation file.
 *  \details    This file implements the BehaviorFollowPathInOccupancyGrid class.
 *  \authors    Guillermo Echegoyen
 *  \maintainer Guillermo Echegoyen
 *  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#include "../include/behavior_follow_path_in_occupancy_grid.h"

BehaviorFollowPathInOccupancyGrid::BehaviorFollowPathInOccupancyGrid() : BehaviorProcess(){}
BehaviorFollowPathInOccupancyGrid::~BehaviorFollowPathInOccupancyGrid(){}

void BehaviorFollowPathInOccupancyGrid::ownSetUp(){
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory, "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack");

  private_nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "EstimatedPose_droneGMR_wrt_GFF");
  private_nh.param<std::string>("follow_path_topic", follow_path_topic, "follow_path");
  private_nh.param<std::string>("trajectory_topic", trajectory_topic, "droneTrajectoryAbsRefCommand");
  private_nh.param<std::string>("consult_belief_service", execute_query_srv, "consult_belief");
  private_nh.param<std::string>("controllers_topic", controllers_topic, "command/high_level");
  private_nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "EstimatedPose_droneGMR_wrt_GFF");

  std::cout<<"ownSetUp"<<std::endl;
}

void BehaviorFollowPathInOccupancyGrid::ownStart(){
  traj_pub = node_handle.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(trajectory_topic, 1);
  path_sub = node_handle.subscribe(follow_path_topic, 1000, &BehaviorFollowPathInOccupancyGrid::followPathCallback, this);
  query_client = node_handle.serviceClient<droneMsgsROS::ConsultBelief>(execute_query_srv);
  estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &BehaviorFollowPathInOccupancyGrid::estimatedPoseCallback, this);
  controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_topic, 1000, true);

  trajectory_sent = false;
  // Force the estimated pose, no progress to be made without it
  /*estimated_pose.x = 0;
  estimated_pose.y = 0;
  estimated_pose.z = 0;
  estimated_pose = *ros::topic::waitForMessage<droneMsgsROS::dronePose>(estimated_pose_str, node_handle);*/
  setupTrajectory(current_traj);
  grabInputPath(current_traj);
  sendTrajectory(current_traj);
}

void BehaviorFollowPathInOccupancyGrid::ownStop(){
  path_sub.shutdown();
  traj_pub.shutdown();
  query_client.shutdown();
  estimated_pose_sub.shutdown();
  controllers_pub.shutdown();
  trajectory_sent = false;
}

void BehaviorFollowPathInOccupancyGrid::ownRun(){
  is_finished = goalReached(estimated_pose, target_position) && trajectory_sent;
  if( is_finished ){
    std::cout<< "Behavior finished WITHOUT error" << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }else if( timerIsFinished() ){
    std::cout<< "Behavior finished WITH error" << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }
}

std::tuple<bool,std::string> BehaviorFollowPathInOccupancyGrid::ownCheckActivationConditions(){
  
  droneMsgsROS::ConsultBelief query_service;
  query_service.request.query = "battery_level(self,LOW)";

  if( query_client.call(query_service) == 0 ){
    std::cout<<"Warning: Belief Query service not ready, try again..."<<std::endl;
  }

  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
  }

  query_service.request.query = "flight_state(self,LANDED)";

  if( query_client.call(query_service) == 0 ){
    std::cout<<"Warning: Belief Query service not ready, try again..."<<std::endl;
  }

  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
  }
  return std::make_tuple(true,"");
}

void BehaviorFollowPathInOccupancyGrid::estimatedPoseCallback(const droneMsgsROS::dronePose& msg){
  estimated_pose = msg;
}

bool BehaviorFollowPathInOccupancyGrid::goalReached(droneMsgsROS::dronePose current, droneMsgsROS::dronePose target){
  /*std::cout<<"Goal "<< fabs(current.x - target.x);
  std::cout<<", "<< fabs(current.y - target.y);
  std::cout<<", "<< fabs(current.z - target.z)<<std::endl;*/
  return (
    fabs(current.x - target.x) < DIST_TOL &&
    fabs(current.y - target.y) < DIST_TOL &&
    fabs(current.z - target.z) < DIST_TOL
    // && fabs(current.yaw - target.yaw) < YAW_TOL
  );
}

bool BehaviorFollowPathInOccupancyGrid::grabInputPath(droneMsgsROS::dronePositionTrajectoryRefCommand &return_path){
  std::string arguments = getArguments();
  YAML::Node config_file = YAML::Load(arguments);
  if(!config_file["path"].IsDefined()){
    std::cout<< "Null path" <<std::endl;
    return false;
  }
  std::string path = config_file["path"].as<std::string>();
  try{
    parseInputPath(path, return_path);
    std::cout<<"Parsed path "<< return_path<<std::endl;
  }catch(const std::exception& e) {
    std::cout<< "Behavior finished WITH error: " << e.what() << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::WRONG_PROGRESS);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }
  return true;
}

void BehaviorFollowPathInOccupancyGrid::setupTrajectory(
  droneMsgsROS::dronePositionTrajectoryRefCommand &return_path
){
  return_path.header.stamp = ros::Time::now();
  return_path.initial_checkpoint = 0;
  return_path.is_periodic = false;
  return_path.droneTrajectory.clear();
}

void BehaviorFollowPathInOccupancyGrid::convertPath(
  const nav_msgs::Path &path,
  droneMsgsROS::dronePositionTrajectoryRefCommand &return_path
){
  for(int i = 0; i < path.poses.size(); i++){
    droneMsgsROS::dronePositionRefCommand next_waypoint;
    next_waypoint.x = path.poses[i].pose.position.x;
    next_waypoint.y = path.poses[i].pose.position.y;
    next_waypoint.z = estimated_pose.z;
    return_path.droneTrajectory.push_back(next_waypoint);
  }
}

void BehaviorFollowPathInOccupancyGrid::parseInputPath(const std::string path, droneMsgsROS::dronePositionTrajectoryRefCommand &return_path){

  std::string::const_iterator path_start = path.begin();
  std::string::const_iterator path_end = path.end();

  // regex to match terns of the path, comes like:
  // ((0.1,0,12), (1.2,1.7,1.7), (2.3,2.9,2.9), (3.4,3.11,3.11))
  //  extract parts like (0.1,0,12)
  boost::regex path_regex("(\\(-?[0-9]+(?:\\.[0-9]+)?,\\s?-?[0-9]+(?:\\.[0-9]+)?,\\s?-?[0-9]+(?:\\.[0-9]+)?\\))", boost::regex_constants::perl);
  boost::regex_constants::match_flag_type flags = boost::match_default;
  boost::smatch path_match;

  // regex to match each part of the vector
  //  extract parts like 0.1
  boost::regex vec_regex("(\\s?-?[0-9]+(?:\\.[0-9]+)?\\s?)");  

  // std::cout<<"Regex "<<path_regex<<" Start "<<std::string(path_start, path_end)<<std::endl;

  while(path_start < path_end){
    if( boost::regex_search(path_start, path_end, path_match, path_regex, flags) ){
      // std::cout << path_match[1] << " size: " << path_match[1].str().size() << std::endl;
      flags |= boost::regex_constants::match_prev_avail;
      flags |= boost::regex_constants::match_not_bob;
      path_start+= path_match[1].str().size();

      std::string str_match = path_match[1].str();

      std::string::const_iterator vec_start = str_match.begin();
      std::string::const_iterator vec_end = str_match.end();
      boost::regex_constants::match_flag_type vec_flags = boost::match_default;
      boost::smatch vec_match;

      std::vector<double> point_vect;

      while( vec_start < vec_end ){
        if( boost::regex_search(vec_start, vec_end, vec_match, vec_regex, vec_flags) ){
          // std::cout<< "Vector match " << vec_match[1] << " size " << vec_match[1].str().size() << std::endl;

          vec_flags |= boost::regex_constants::match_prev_avail;
          vec_flags |= boost::regex_constants::match_not_bob;
          vec_start = vec_start + (vec_match[1].str().size() + 1);
          point_vect.push_back(std::stof(vec_match[1].str()));
        }else{
          vec_start += 1;
        }
      }
      if (point_vect.size() == 3){
        droneMsgsROS::dronePositionRefCommand point;
        point.x = point_vect[0];
        point.y = point_vect[1];
        point.z = point_vect[2];
        return_path.droneTrajectory.push_back(point);
      }else{
        throw std::runtime_error("malformed path");
      }
    }else{
      path_start+= 1;
    }
  }
}

void BehaviorFollowPathInOccupancyGrid::setControlMode(){
  // set trajectory controller mode to move and wait for the change
  droneMsgsROS::droneCommand msg;
  std_msgs::Header header;
  header.frame_id = "behavior_follow_path_in_occupancy_grid";
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);

  // Wait for controller to change mode
  ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
    "droneTrajectoryController/controlMode", node_handle
  );
}

void BehaviorFollowPathInOccupancyGrid::sendTrajectory(droneMsgsROS::dronePositionTrajectoryRefCommand &traj){
  std::cout<<"Sending controle mode "<< std::endl;
  setControlMode();
  std::cout<<"Sending controle mode "<< std::endl;
  traj_pub.publish(traj);
  
  // store the target position
  droneMsgsROS::dronePositionRefCommand target_pose = traj.droneTrajectory[traj.droneTrajectory.size()-1];
  target_position.x = target_pose.x;
  target_position.y = target_pose.y;
  target_position.z = target_pose.z;

  std::cout<<"Trajectory sent with (" << traj.droneTrajectory.size() << ") points"<<std::endl;
  trajectory_sent = true;
}

void BehaviorFollowPathInOccupancyGrid::followNavPathCallback(const nav_msgs::Path &path){
  setupTrajectory(current_traj);
  convertPath(path, current_traj);
  sendTrajectory(current_traj);
}

void BehaviorFollowPathInOccupancyGrid::followPathCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand &path){
  std::cout<<"Received path to follow, size(" <<  path.droneTrajectory.size() << ")" << std::endl;
  setupTrajectory(current_traj);
  current_traj.droneTrajectory = path.droneTrajectory;
  sendTrajectory(current_traj);
}

