/*!*******************************************************************************************
*  \file       behavior_follow_path_in_occupancy_grid.h
*  \brief      behavior follow path in occupancy grid definition file.
*  \details     This file contains the BehaviorFollowPathInOccupancyGrid declaration. To obtain more information about
*              it's definition consult the behavior_follow_path_in_occupancy_grid.cpp file.
*  \authors    Guillermo Echegoyen
*  \maintainer Guillermo Echegoyen
*  \copyright  Copyright 2018 Universidad Politecnica de Madrid (UPM)
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program. If not, see http://www.gnu.org/licenses/.
********************************************************************************/
#ifndef BEHAVIOR_FOLLOW_PATH_IN_OCCUPANCY_GRID_H
#define BEHAVIOR_FOLLOW_PATH_IN_OCCUPANCY_GRID_H

// ROS
#include <ros/ros.h>

#include <std_msgs/String.h>
#include <behavior_process.h>

#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>
#include <droneMsgsROS/dronePositionRefCommand.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/dronePose.h>
#include <nav_msgs/Path.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <boost/regex.hpp>
#include <stdexcept>
#include <yaml-cpp/yaml.h>

class BehaviorFollowPathInOccupancyGrid : public BehaviorProcess
{
public:
  BehaviorFollowPathInOccupancyGrid();
  ~BehaviorFollowPathInOccupancyGrid();

private:
  const double DIST_TOL = 0.2;

  // publishers, subscribers
  ros::NodeHandle node_handle;
  ros::Publisher traj_pub;
  ros::Publisher controllers_pub;
  ros::Subscriber path_sub;
  ros::Subscriber estimated_pose_sub;
  ros::ServiceClient query_client;

  droneMsgsROS::dronePositionTrajectoryRefCommand current_traj;
  droneMsgsROS::dronePose estimated_pose;
  droneMsgsROS::dronePose target_position;

  //Congfig variables
  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;

  std::string estimated_pose_str;
  std::string follow_path_topic;
  std::string trajectory_topic;
  std::string execute_query_srv;
  std::string controllers_topic;

  bool is_finished;
  bool trajectory_sent;

  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
  std::tuple<bool,std::string> ownCheckActivationConditions();

  // Callbacks
  // accept either an aerostack trajectory or a navigation one
  void followPathCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand &path);
  void followNavPathCallback(const nav_msgs::Path &path);
  void estimatedPoseCallback(const droneMsgsROS::dronePose& msg);

  // Utils
  void setupTrajectory(droneMsgsROS::dronePositionTrajectoryRefCommand &return_path);
  void sendTrajectory(droneMsgsROS::dronePositionTrajectoryRefCommand &traj);
  void setControlMode();
  void convertPath(const nav_msgs::Path &path, droneMsgsROS::dronePositionTrajectoryRefCommand &return_path);
  void parseInputPath(const std::string path, droneMsgsROS::dronePositionTrajectoryRefCommand &return_path);
  bool grabInputPath(droneMsgsROS::dronePositionTrajectoryRefCommand &return_path);
  bool goalReached(const droneMsgsROS::dronePose current, const droneMsgsROS::dronePose target);
};

#endif
